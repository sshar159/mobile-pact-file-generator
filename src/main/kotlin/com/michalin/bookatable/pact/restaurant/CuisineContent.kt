package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.apache.http.entity.ContentType

class CuisineContent(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {

    override val responseBody: DslPart?
        get() {
            return newJsonBody { body ->
                body
                    .eachLike("changes", 1) { change ->
                        change
                            .stringType("id", "8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6")
                            .stringType("translation", "Oxo Tower Restaurant")
                    }
                    .eachLike("deleted") { deleted ->
                        deleted
                            .stringType("id")
                            .and("deleted", true, TypeMatcher)
                    }
                    .stringType("lastSeq", "SEQ-132232")
                    .and("pending", 1, TypeMatcher)
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Cuisine content changes information found"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to retrieve Cuisine content changes information")
            .matchPath("^\\/v1\\/content\\/cuisines\\/changes", "/v1/content/cuisines/changes")
            .query("limit=20&since=0&languageCode=en-GB")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        return null
    }
}