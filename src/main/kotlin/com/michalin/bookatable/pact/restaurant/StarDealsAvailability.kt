package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import org.apache.http.entity.ContentType

class StarDealsAvailability(pactDslWithProvider: PactDslWithProvider) :
    MultiRestaurantAvailability(pactDslWithProvider) {

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Star Deals are available"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive available Star Deals")
            .path("/v1/stardeals/availability")
            .matchPath("\\/v1\\/stardeals\\/availability")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .status(200)
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        return null
    }
}