package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody

class LiveAvailability(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .stringType("id", "136379")
                    .stringMatcher("availabilityResult", "(Yes|No|Unknown)", "Yes")
                    .eachLike("areas") { area ->
                        area
                            .stringType("id", "41268")
                            .stringType("name", "Restaurant")
                            .eachLike("sessions", 2) { session ->
                                session
                                    .stringType("name", "LUNCH")
                                    .eachLike("slots", 2) { slot ->
                                        slot
                                            .booleanType("isAlaCarteAvailable", true)
                                            .`object`("time") { time ->
                                                time.stringType("local", "2018-08-07T12:00:00")
                                            }
                                            .eachLike("offers") { offer ->
                                                offer
                                                    .stringType("id", "79740")
                                                    .and("order", 1, TypeMatcher)
                                                    .stringType("name", "Free dessert with your main course")
                                            }
                                    }
                            }
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Live availability found for requested restaurant"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive live availability for a restaurant (Success)")
            .path("/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6/availability")   //8a1a86b2-8888-4433-8833-5a7ceb6755b6
            .query("date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=2&languageCode=en-GB&partnerCode=CHA-ID_10")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        var jsonBodyErrors = newJsonBody { errors ->
            errors.eachLike("errors") { error ->
                error.stringType("code", "PH121").stringType("message", "Restaurant not found for request")
            }
        }.build()

        val given = "Live availability not found for requested restaurant"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)

        val response = pactDslWithState
            .uponReceiving("Request to receive live availability for a existing restaurant but it is not available for this request")
            .path("/v1/restaurant/8a1a86b2-8888-4444-8833-5a7ceb6755b6/availability")
            .query("date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=2&languageCode=en-GB&partnerCode=CHA-ID_10")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(404)
            .body(jsonBodyErrors)

        jsonBodyErrors = newJsonBody { errors ->
            errors.eachLike("errors") { error ->
                error.stringType("code", "PH120").stringType("message", "The requested party size is not valid")
            }
        }.build()

        return response.given("Bad request for Live availability")
            .uponReceiving("Request to receive live availability for a restaurant with invalid party size '-5'")
            .path("/v1/restaurant/8a1a86b2-8888-4444-7744-5a7ceb6755b6/availability")
            .query("date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=-5&languageCode=en-GB&partnerCode=CHA-ID_10")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(400)
            .body(jsonBodyErrors)
    }
}