package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.apache.http.entity.ContentType

class RestaurantFullContent(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .stringType("id", "8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6")
                    .stringType("name", "Oxo Tower Restaurant")
                    .stringType("description", "An iconic landmark on the South Bank")
                    .stringType("rev", "2-f20813fad29df6b2aa71df9dd21cd6f7")
                    .`object`("details") { details ->
                        details
                            .`object`("address") { address ->
                                address
                                    .stringType("city", "London")
                                    .stringType("country", "United Kingdom")
                                    .stringType("line1", "OXO Tower Wharf, Barge House Street")
                                    .stringType("line2", "South Bank")
                                    .stringType("postCode", "SE1 9PH")
                            }
                            //						.and("avgMain", 45.4, TypeMatcher)
                            //						.and("avgMealSpend", 90.50, TypeMatcher)
                            .and("avgRating", 4.5, TypeMatcher)
                            .stringType("contactPhone", "+44 2078033888")
                            .stringType("fullAddress", "OXO Tower Wharf")
                            .and("offersCount", 5, TypeMatcher)
                            .stringType("openingHours", "Monday to Friday")
                            .stringType("reservationsPhone", "+44 2078033888")
                            .and("reviewsCount", 3000, TypeMatcher)
                            .and("starDealsCount", 3, TypeMatcher)
                    }
                    .`object`("geo") { geo ->
                        geo
                            .and("latitude", 38.898648, TypeMatcher)
                            .and("longitude", 77.037692, TypeMatcher)
                    }
                    .`object`("traits") { traits ->
                        traits
                            .and("bookable", true, TypeMatcher)
                    }
                    .minArrayLike("cuisineIds", 1) { cuisineId ->
                        cuisineId
                            .stringType("id", "f5e09433-8493-4a51-9d3e-f49321115203")
                            .and("order", 1, TypeMatcher)
                    }
                    .eachLike("images", 1) { image ->
                        image
                            .stringType("id", "726f6994-2965-4334-8f96-a4b068692483")
                            .and("order", 1, TypeMatcher)
                            .stringType("type", "Something")
                    }
                    .minArrayLike("offers", 0, 2) { offer ->
                        offer
                            .stringType("id", "114345")
                            .and("isStarDeal", true, TypeMatcher)
                            .stringType("name", "Sunday roast")
                            .stringType("description", "Offer: Pizza and Prosecco for -�15 every Tuesday dinner")
                            .and("order", 1, TypeMatcher)
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Restaurant exists with id '83f9262f28f14703ab1a8cfd9e8249c9'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState!!.given(given)
            .uponReceiving("Request to retrieve details of existing restaurant with id '83f9262f28f14703ab1a8cfd9e8249c9'")
            .path("/v1/content/details/83f9262f28f14703ab1a8cfd9e8249c9")
            .query("languageCode=en-GB")
            .matchPath("\\/v1\\/content\\/details\\/83f9262f28f14703ab1a8cfd9e8249c9")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=utf-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        val jsonBodyErrors = newJsonBody { errors ->
            errors!!.eachLike(
                "errors"
            ) { error -> error!!.stringType("code", "404").stringMatcher("message", "^missing|deleted$", "missing") }
        }.build()

        val given = "Missing/deleted restaurant with id '99f9999f99f99999ab9a9cfd9e9999c9'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)

        return pactDslWithState!!
            .uponReceiving("Request for non-existing restaurent with id '99f9999f99f99999ab9a9cfd9e9999c9'")
            .path("/v1/content/details/99f9999f99f99999ab9a9cfd9e9999c9")
            .query("languageCode=en-GB")
            .matchPath("\\/v1\\/content\\/details\\/99f9999f99f99999ab9a9cfd9e9999c9")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=utf-8")
            .status(404)
            .body(jsonBodyErrors)
    }
}