package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.apache.http.entity.ContentType

open class MultiRestaurantAvailability(pactDslWithProvider: PactDslWithProvider) :
    PactDslResponseBuilder(pactDslWithProvider) {

    override val requestBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .array("restaurantIds") { restaurantId ->
                        restaurantId
                            .stringType("8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6")
                    }
                    .stringType("dateFrom", "2019-01-01")
                    .stringType("dateTo", "2019-01-02")
                    .stringType("timeFrom", "10:00")
                    .stringType("timeTo", "09:59")
                    .and("partySize", 2, TypeMatcher)
                    .stringType("languageCode", "en-GB")
                    .stringType("partnerCode", "CHA-ID_10")
            }.build()
        }

    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .eachLike("result") { result ->
                        result
                            .stringType("id", "100001")
                            .stringType("name", "New China")
                            .stringMatcher("availabilityResult", "(Yes|No|Unknown)", "Yes")
                            .eachLike("areas") { area ->
                                area
                                    //.stringType("id", "Restaurangen_id")
                                    .stringType("name", "Restaurangen")
                                    .eachLike("sessions") { session ->
                                        session
                                            .stringType("name", "LUNCH")
                                            .eachLike("slots", 2) { slot ->
                                                slot
                                                    .booleanType("isAlaCarteAvailable", true)
                                                    .`object`("time") { time ->
                                                        time
                                                            .stringType("local", "2018-08-07T12:00:00")
                                                    }
                                                    .eachLike("offers") { offer ->
                                                        offer
                                                            .stringType("id", "162550")
                                                            .and("order", 1, TypeMatcher)
                                                            .stringType(
                                                                "name",
                                                                "- 3 courses & a glass of prosecco -$34 per person"
                                                            )
                                                    }
                                            }
                                    }
                            }
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "MultiRestaurantAvailability API (MRAS) is hit by mobile app"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to check availability in multipal restaurants (MRAS)")
            .path("/v1/availability")
            .matchPath("\\/v1\\/availability")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .status(200)
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        return null
    }
}