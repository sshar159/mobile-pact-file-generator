package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.apache.http.entity.ContentType

class RestaurantLiteContent(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {

    override val responseBody: DslPart?
        get() {
            val jsonBody = newJsonBody { body ->
                body
                    .eachLike("changes", 1) { change ->
                        change
                            .stringType("id", "8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6")
                            .stringType("name", "Oxo Tower Restaurant")
                            .and("latitude", 38.898648, TypeMatcher)
                            .and("longitude", 77.037692, TypeMatcher)
                            //						.stringType("imageName", "Image-919192323")
                            .stringType("country", "United Kingdom")
                            //						.and("avgMealSpend", 90, TypeMatcher)
                            //						.and("avgMain", 45, TypeMatcher)
                            .eachLike("cuisineIds") { cuisineId ->
                                cuisineId
                                    .stringType("id", "8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6")
                                    .and("order", 1, TypeMatcher)
                            }
                            .stringType("address", "address")
                            .and("avgRating", 4.5, TypeMatcher)
                            .and("reviewsCount", 3000, TypeMatcher)
                            .and("offersCount", 5, TypeMatcher)
                            .and("starDealsCount", 3, TypeMatcher)
                            .and("bookable", true, TypeMatcher)
                    }
                    .eachLike("deleted") { deleted ->
                        deleted
                            .stringType("id")
                            .and("deleted", true, TypeMatcher)
                    }
                    .stringType("lastSeq", "SEQ-132232")
                    .and("pending", 1, TypeMatcher)
            }.build()
            return jsonBody
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "List of Restaurant Changes found"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to retrieve list of Restaurant Changes")
            .matchPath("^\\/v1\\/content\\/summary\\/changes", "/v1/content/summary/changes")
            .query("limit=20&languageCode=en-GB&since=0")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        return null
    }
}