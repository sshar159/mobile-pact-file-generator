package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody

class AvailabilitySettings(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
//    private val calendar = Calendar.getInstance()!!
//    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
//    private val fromDate = System.currentTimeMillis()
//    private val fromDateString = dateFormat.format(fromDate)!!

    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .`object`("availabilitySettings") { availabilitySettings ->
                        availabilitySettings
                            .booleanType("acceptsSpecialRequests", true)
                            .and("daysInAdvance", 90, TypeMatcher)
                            .array("excludedDates") { date ->
                                date
                                    .stringType("2027-10-07T00:00:00")
                                    .stringType("2027-10-14T00:00:00")
                            }
                            .and("maxPartySize", 20, TypeMatcher)
                            .and("minPartySize", 2, TypeMatcher)
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
//        calendar.set(Calendar.YEAR, 2027)
//        val toDate = calendar.time
//        val toDateString = dateFormat.format(toDate)
        val given = "Availability Settings are available for requested restaurant"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive availability settings (Available) for a restaurant")
            .path("/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6/availabilitysettings")
            .query("languageCode=en-GB&fromDate=2027-10-01&toDate=2027-10-30&partnerCode=CHA-ID_10")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        val jsonBodyErrors = newJsonBody { errors ->
            errors
                .eachLike("errors") { error ->
                    error.stringType("code", "PH131").stringType("message", "Availability settings not found")
                }
        }.build()

//        calendar.add(Calendar.MONTH, 1)
//        val toDate = calendar.time
//        val toDateString = dateFormat.format(toDate)
        val given = "Availability Settings are not available for requested restaurant"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive availability settings (Non-Available) for a restaurant")
            .path("/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb5567b6/availabilitysettings")
            .query("languageCode=en-GB&fromDate=2027-09-01&toDate=2027-09-30&partnerCode=CHA-ID_10")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(404)
            .body(jsonBodyErrors)
    }
}