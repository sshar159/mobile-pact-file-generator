package com.michalin.bookatable.pact.booking

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody

class CancelBooking(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {

    override val requestBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .stringType("bookingStatus", "Cancelled")
                    .stringMatcher("emailAddress", "(.+)@(.+)", "customer@bookatable.com")
                    .stringMatcher("languageCode", "^(([a-z]{2}-[A-Z]{2})|[a-z]{2})$", "en-GB")
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Booking details are available for booking id #530987421"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to cancel existing booking with id #530987421 - (version: v1)")
            .path("/v1/bookings/530987421")
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .status(204)
            .given("Booking details are available for booking confirmation #FLAH001")
            .uponReceiving("Request to cancel existing booking with confirmation #FLAH001 - (version: v2)")
            .path("/v2/bookings/FLAH001")
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .status(204)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        val jsonBodyErrors = newJsonBody { errors ->
            errors.eachLike("errors") { error ->
                error.stringType("code", "404").stringType("message", "Booking doesn't exist")
            }
        }.build()

        val given = "Booking details not available for booking id 530987422"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to cancel non-existing booking with id #530987422 (version: v1)")
            .path("/v1/bookings/530987422")
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(404)
            .body(jsonBodyErrors)
            .given("Booking details not available for booking confirmation #FLAH002")
            .uponReceiving("Request to cancel non-existing booking with confirmation #FLAH002 (version: v2)")
            .path("/v2/bookings/FLAH002")
            .method("POST")
            .body(requestBody)
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(404)
            .body(jsonBodyErrors)
    }
}