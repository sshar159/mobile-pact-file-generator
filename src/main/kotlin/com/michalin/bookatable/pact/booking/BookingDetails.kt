package com.michalin.bookatable.pact.booking

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonArray
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonArrayMinLike

class BookingDetails(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
    override val responseBody: DslPart?
        get() {
            return newJsonArrayMinLike(1) { booking ->
                booking
                    .`object` { bookingId ->
                        bookingId
                            .and("bookingId", 530987421, TypeMatcher)
                            .stringType("bookingStatus", "Confirmed")
                            .stringType("confirmationNumber", "FLAH001")
                            .stringType("diningDateTime", "2018-08-12T20:30:00")
                            .booleanType("isCancellable", true)
                            .and("partySize", 4, TypeMatcher)
                            .stringType("restaurantName", "Trattoria Milano")
                            .stringType("sessionId", "Dinner")
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Booking details available for email 'customer@bookatable.com' and confirmation no. 'FLAH001'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to get existing booking details for email 'customer@bookatable.com' and confirmation no. 'FLAH001'")
            .matchPath("/v1/bookings", "/v1/bookings")
            .query("emailAddress=customer@bookatable.com&confirmationNumber=FLAH001")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Booking details not available for email 'customer@bookatable.com' and confirmation no. 'FLAH002'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to get non-existing booking details for email 'customer@bookatable.com' and confirmation no. 'FLAH002'")
            .matchPath("/v1/bookings", "/v1/bookings")
            .query("emailAddress=customer@bookatable.com&confirmationNumber=FLAH002")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(newJsonArray { }.build())
    }
}