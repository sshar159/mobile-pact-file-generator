package com.michalin.bookatable.pact.offer

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody
import org.apache.http.entity.ContentType

class Offers(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    .eachLike("result", 5) { result ->
                        result
                            .stringType("id", "100001")
                            .stringType("name", "Offer Name")
                            .stringType("menuLink", "Menu Link")
                            .stringType("description", "Description")
                            .stringType("marketingFromDate", "2018-08-07T12:00:00")
                            .stringType("marketingToDate", "2018-08-17T12:00:00")
                    }
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "Offers are available"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive available offers for languageCode 'en-GB'")
            .path("/v1/offers")
            .query("languageCode=en-GB")
            .matchPath("^\\/v1\\/offers")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("POST")
            .willRespondWith()
            .status(200)
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        val jsonBodyErrors = newJsonBody { errors ->
            errors.eachLike("errors") { error ->
                error
                    .stringType("code", "404")
                    .stringType("message", "No offers available for languageCode 'en-IN'")
            }
        }.build()
        val given = "No offers available for languageCode 'en-IN'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to receive offers for languageCode 'en-IN'")
            .path("/v1/offers")
            .query("languageCode=en-IN")
            .matchPath("^\\/v1\\/offers")
            .matchHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .method("POST")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(404)
            .body(jsonBodyErrors)
    }
}