package com.michalin.bookatable.pact

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider

abstract class PactDslResponseBuilder(pactDslWithProvider: PactDslWithProvider) {

    protected val pactDslWithProvider: PactDslWithProvider = pactDslWithProvider

    open val requestBody: DslPart?
        get() {
            return null
        }

    open val responseBody: DslPart?
        get () {
            return null
        }

    abstract fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse?
    abstract fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse?
}