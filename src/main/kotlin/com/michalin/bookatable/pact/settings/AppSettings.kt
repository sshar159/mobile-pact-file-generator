package com.michalin.bookatable.pact.settings

import au.com.dius.pact.consumer.dsl.DslPart
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.model.matchingrules.TypeMatcher
import com.michalin.bookatable.pact.PactDslResponseBuilder
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody

class AppSettings(pactDslWithProvider: PactDslWithProvider) : PactDslResponseBuilder(pactDslWithProvider) {
    override val responseBody: DslPart
        get() {
            return newJsonBody { body ->
                body
                    /*.eachLike("changes", 1) { change ->
                        change*/
                            .stringType("id", "8a9872e7670e460e8dbbde54b5eb63c2")
                            .stringType("rev", "1-aca58f548ed524aeda9450ba8fabf796")
                            .stringType("vendor", "Apple")
                            .booleanType("forceAppUpdate", true)
                            .`object`("forceUpdateInfo") { forceUpdateInfo ->
                                forceUpdateInfo
                                    .eachLike("localizedContent", 1) { localizedContent ->
                                        localizedContent
                                            .`object`("content") { content ->
                                                content
                                                    .stringType(
                                                        "body",
                                                        "Your App needs to be updated to function properly."
                                                    )
                                                    .stringType("buttonTitle", "Update now")
                                                    .stringType("title", "Sorry we no longer support this version")
                                            }
                                            .stringType("languageCode", "en-GB")
                                    }
                                    .array("versions") { version ->
                                        version
                                            .stringType("2.0")
                                            .stringType("2.1")
                                            .stringType("3.0")
                                    }
                                    .stringType("url", "itms://itunes.apple.com/gb/app/bookatable/id402188354?mt=8")
                            }
                /*}
                .eachLike("deleted") { deleted ->
                    deleted
                        .stringType("id")
                        .stringType("seq")
                        .stringType("rev")
                        .booleanType("deleted")
                }
                .stringType("lastSeq", "SEQ-132232")
                .and("pending", 1, TypeMatcher)*/
            }.build()
        }

    override fun prepareResponseForSuccess(defaultResponse: PactDslResponse?): PactDslResponse? {
        val given = "App Settings are available for App ID = 'app-id12345'"
        val pactDslWithState = defaultResponse?.given(given) ?: pactDslWithProvider.given(given)
        return pactDslWithState
            .uponReceiving("Request to retrieve App Settings")
            .path("/v1/appsettings/appId-12345")
            .query("languageCode=en-GB")
            .method("GET")
            .willRespondWith()
            .matchHeader("Content-Type", "^application\\/json; charset=(utf|UTF)-8$", "application/json; charset=UTF-8")
            .status(200)
            .body(responseBody)
    }

    override fun prepareResponseForFailure(defaultResponse: PactDslResponse?): PactDslResponse? {
        return null
    }
}