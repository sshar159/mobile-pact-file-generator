package com.michalin.bookatable.pact.booking

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "Cancel Booking API Endpoint", port = "20432", pactVersion = PactSpecVersion.V2)
class CancelBookingsConsumerPactTest : AbstractConsumerPactTest() {

    private lateinit var cancelBooking: CancelBooking

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
    }

    @Pact(provider = "Cancel Booking API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        cancelBooking = CancelBooking(pactDslWithProvider)
        var response = cancelBooking.prepareResponseForFailure(null)
        response = cancelBooking.prepareResponseForSuccess(response)

        val pact = response!!.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        /**
         * Cancel Requests (version: v1) to test cancel scenario
         */
        var httpResponse = Request.Post(mockServer.getUrl() + "/v1/bookings/530987421")
            .bodyString(cancelBooking.requestBody.toString(), ContentType.APPLICATION_JSON)
            .execute().returnResponse()

        assertThat(httpResponse!!.statusLine.statusCode, `is`(equalTo(204)))

        httpResponse = Request.Post(mockServer.getUrl() + "/v1/bookings/530987422")
            .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .bodyString(cancelBooking.requestBody.toString(), ContentType.APPLICATION_JSON)
            .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(404)))

        /**
         * Cancel Requests (version: v1) to test cancel scenario
         */
        httpResponse = Request.Post(mockServer.getUrl() + "/v2/bookings/FLAH001")
            .bodyString(cancelBooking.requestBody.toString(), ContentType.APPLICATION_JSON)
            .execute().returnResponse()

        assertThat(httpResponse!!.statusLine.statusCode, `is`(equalTo(204)))

        httpResponse = Request.Post(mockServer.getUrl() + "/v2/bookings/FLAH002")
            .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .bodyString(cancelBooking.requestBody.toString(), ContentType.APPLICATION_JSON)
            .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(404)))
    }
}