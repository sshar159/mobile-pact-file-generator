package com.michalin.bookatable.pact.booking

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import io.pactfoundation.consumer.dsl.LambdaDsl.newJsonArray
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "Booking Details API Endpoint", port = "20424", pactVersion = PactSpecVersion.V2)
class BookingDetailsConsumerPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("emailAddress=(.+)%40(.+)&confirmationNumber=[A-Z]{4}[0-9]{3}"))
    }

    @Pact(provider = "Booking Details API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val responseBuilder = BookingDetails(pactDslWithProvider)
        var response = responseBuilder.prepareResponseForFailure(null)
        response = responseBuilder.prepareResponseForSuccess(response)

        val pact = response!!.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        var httpResponse =
            Request.Get(mockServer.getUrl() + "/v1/bookings?emailAddress=customer@bookatable.com&confirmationNumber=FLAH001")
                .addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))

        httpResponse =
            Request.Get(mockServer.getUrl() + "/v1/bookings?emailAddress=customer@bookatable.com&confirmationNumber=FLAH002")
                .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
                .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))

        val responseBody = httpResponse.entity.content.bufferedReader().use { it.readText() }
        println(responseBody)
        assertThat(responseBody, `is`(equalTo(newJsonArray { }.build().toString())))
    }
}