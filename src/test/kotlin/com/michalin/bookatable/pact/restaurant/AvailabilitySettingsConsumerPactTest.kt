package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "Availability Settings API Endpoint", port = "20427", pactVersion = PactSpecVersion.V2)
class AvailabilitySettingsConsumerPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("languageCode=[a-z]{2}-[A-z]{2}", "languageCode=en-GB"))
    }

    @Pact(provider = "Availability Settings API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val availabilitySettings = AvailabilitySettings(pactDslWithProvider)
        var response = availabilitySettings.prepareResponseForSuccess(null)
        response = availabilitySettings.prepareResponseForFailure(response)
        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
//        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
//        val fromDate = System.currentTimeMillis()
//        val fromDateString = dateFormat.format(fromDate)
//        val calendar = Calendar.getInstance()
//        calendar.add(Calendar.MONTH, 10)
//        var toDate = calendar.time
//        var toDateString = dateFormat.format(toDate)

        var httpResponse = Request.Get(
            mockServer.getUrl() + "/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6/availabilitysettings"
                    + "?languageCode=en-GB&fromDate=2027-10-01&toDate=2027-10-30&partnerCode=CHA-ID_10"
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))

//        calendar.add(Calendar.MONTH, 1)
//        toDate = calendar.time
//        toDateString = dateFormat.format(toDate)
        httpResponse = Request.Get(
            mockServer.getUrl() + "/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb5567b6/availabilitysettings"
                    + "?languageCode=en-GB&fromDate=2027-09-01&toDate=2027-09-30&partnerCode=CHA-ID_10"
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(404)))
    }
}