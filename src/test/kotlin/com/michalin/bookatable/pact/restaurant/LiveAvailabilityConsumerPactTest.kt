package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "Live Availability API Endpoint", port = "20429", pactVersion = PactSpecVersion.V2)
class LiveAvailabilityConsumerPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("languageCode=[a-z]{2}-[A-z]{2}", "languageCode=en-GB"))
    }

    @Pact(provider = "Live Availability API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val liveAvailability = LiveAvailability(pactDslWithProvider)
        var response = liveAvailability.prepareResponseForSuccess(null)
        response = liveAvailability.prepareResponseForFailure(response)
        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        var httpResponse = Request.Get(
            mockServer.getUrl() + "/v1/restaurant/8a1a86b2-8ef5-42b7-8833-5a7ceb6755b6/availability"
                    + "?date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=2" +
                    "&languageCode=en-GB&partnerCode=CHA-ID_10"
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))

        httpResponse = Request.Get(
            (mockServer.getUrl() + "/v1/restaurant/8a1a86b2-8888-4444-8833-5a7ceb6755b6/availability"
                    + "?date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=2&languageCode=en-GB&partnerCode=CHA-ID_10")
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(404)))

        httpResponse = Request.Get(
            (mockServer.getUrl() + "/v1/restaurant/8a1a86b2-8888-4444-7744-5a7ceb6755b6/availability"
                    + "?date=2017-12-19&timeFrom=15:00&timeTo=15:30&partySize=-5&languageCode=en-GB&partnerCode=CHA-ID_10")
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(400)))
    }
}