package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "MRAS API Endpoint", port = "20426", pactVersion = PactSpecVersion.V2)
class MultiRestaurantAvailabilityConsumerPactTest : AbstractConsumerPactTest() {

    private lateinit var multiRestaurantAvailability: MultiRestaurantAvailability

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
    }


    @Pact(provider = "MRAS API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        multiRestaurantAvailability = MultiRestaurantAvailability(pactDslWithProvider)
        val response = multiRestaurantAvailability.prepareResponseForSuccess(null)
        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        val httpResponse = Request.Post(mockServer.getUrl() + "/v1/availability")
            .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
            .bodyString(multiRestaurantAvailability.requestBody.toString(), ContentType.APPLICATION_JSON)
            .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))
    }
}