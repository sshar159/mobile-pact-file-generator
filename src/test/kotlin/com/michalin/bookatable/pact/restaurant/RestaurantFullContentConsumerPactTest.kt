package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "RestaurantFullContent API Endpoint", port = "20422", pactVersion = PactSpecVersion.V2)
class RestaurantFullContentConsumerPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("languageCode=[a-z]{2}-[A-z]{2}", "languageCode=en-GB"))
    }

    @Pact(provider = "RestaurantFullContent API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val restaurantFullContent = RestaurantFullContent(pactDslWithProvider)
        var response = restaurantFullContent.prepareResponseForSuccess(null)
        response = restaurantFullContent.prepareResponseForFailure(response)

        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun runTest(mockServer: MockServer) {
        var httpResponse =
            Request.Get(mockServer.getUrl() + "/v1/content/details/83f9262f28f14703ab1a8cfd9e8249c9?languageCode=en-GB")
                .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
                .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))

        httpResponse =
            Request.Get(mockServer.getUrl() + "/v1/content/details/99f9999f99f99999ab9a9cfd9e9999c9?languageCode=en-GB")
                .addHeader("Accept", ContentType.APPLICATION_JSON.toString())
                .execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(404)))
    }
}