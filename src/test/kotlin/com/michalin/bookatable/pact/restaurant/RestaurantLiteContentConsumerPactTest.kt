package com.michalin.bookatable.pact.restaurant

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "Restaurant Lite Content API Endpoint", port = "20430", pactVersion = PactSpecVersion.V2)
class RestaurantLiteContentConsumerPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("languageCode=[a-z]{2}-[A-z]{2}", "languageCode=en-GB"))
    }

    @Pact(provider = "Restaurant Lite Content API Endpoint", consumer = PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val restaurantLiteContent = RestaurantLiteContent(pactDslWithProvider)
        var response = restaurantLiteContent.prepareResponseForFailure(null)
        response = restaurantLiteContent.prepareResponseForSuccess(response)
        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        val httpResponse = Request.Get(
            mockServer.getUrl() + "/v1/content/summary/changes?limit=20&since=0&languageCode=en-GB"
        ).addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))
    }
}