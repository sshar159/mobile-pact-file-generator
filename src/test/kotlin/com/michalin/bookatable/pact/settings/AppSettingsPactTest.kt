package com.michalin.bookatable.pact.settings

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.Pact
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.model.PactSpecVersion
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.RegexMatcher
import com.michalin.bookatable.pact.AbstractConsumerPactTest
import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import java.io.IOException

@PactTestFor(providerName = "App Settings API Endpoint", port = "20428", pactVersion = PactSpecVersion.V2)
class AppSettingsPactTest : AbstractConsumerPactTest() {

    override fun applyMatchingRulesForRequestQuery(request: au.com.dius.pact.model.Request) {
        val queryMatchers = request.matchingRules?.addCategory("query")
        queryMatchers?.addRule(RegexMatcher("languageCode=[a-z]{2}-[A-z]{2}", "languageCode=en-GB"))
    }

    @Pact(provider = "App Settings API Endpoint", consumer = AbstractConsumerPactTest.PACT_CONSUMER)
    fun createPact(pactDslWithProvider: PactDslWithProvider): RequestResponsePact? {
        val responseBuilder = AppSettings(pactDslWithProvider)
        val response = responseBuilder.prepareResponseForSuccess(null)
        val pact = response?.toPact()
        modifyMatchingRules(pact)
        return pact
    }

    @Test
    @Throws(IOException::class)
    fun test(mockServer: MockServer) {
        val httpResponse = Request.Get(mockServer.getUrl() + "/v1/appsettings/appId-12345?languageCode=en-GB")
            .addHeader("Accept", ContentType.APPLICATION_JSON.toString()).execute().returnResponse()

        assertThat(httpResponse.statusLine.statusCode, `is`(equalTo(200)))
    }
}