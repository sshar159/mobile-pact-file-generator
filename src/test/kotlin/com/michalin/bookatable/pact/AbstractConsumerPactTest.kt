package com.michalin.bookatable.pact

import au.com.dius.pact.consumer.DefaultRequestValues
import au.com.dius.pact.consumer.DefaultResponseValues
import au.com.dius.pact.consumer.dsl.PactDslRequestWithoutPath
import au.com.dius.pact.consumer.dsl.PactDslResponse
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.model.Request
import au.com.dius.pact.model.RequestResponsePact
import au.com.dius.pact.model.matchingrules.MatchingRules
import au.com.dius.pact.model.matchingrules.MatchingRulesImpl
import org.apache.commons.collections4.MapUtils
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(PactConsumerTestExt::class)
abstract class AbstractConsumerPactTest {

    private fun modifiedMatchingRules(matchingRules: MatchingRules?): MatchingRules {
        val matchingRulesImpl = matchingRules as MatchingRulesImpl
        val modifiedMatchingRules = MatchingRulesImpl()

        matchingRulesImpl.rules.forEach { key, category ->
            if ("header" == key) {
                val headers = modifiedMatchingRules.addCategory("headers")
                category.matchingRules.forEach { item, matchingRule -> headers.addRules(item, matchingRule.rules) }
            } else if ("query" != key) {
                modifiedMatchingRules.addCategory(category)
            }
        }
        return modifiedMatchingRules
    }

    protected fun modifyMatchingRules(pact: RequestResponsePact?) {
        pact?.interactions?.forEach { interaction ->
            val request = interaction.request
            request.matchingRules = modifiedMatchingRules(request.matchingRules)
            applyMatchingRulesForRequestQuery(request)

            val response = interaction.response
            response.matchingRules = modifiedMatchingRules(response.matchingRules)
        }
    }

    protected abstract fun applyMatchingRulesForRequestQuery(request: Request)

    @DefaultRequestValues
    fun defaultRequestValues(request: PactDslRequestWithoutPath?) {
        request!!.headers(DEFAULT_REQUEST_HEADERS)
    }

    @DefaultResponseValues
    fun defaultResponseValues(response: PactDslResponse?) {
        response!!.headers(DEFAULT_RESPONSE_HEADERS)
    }

    companion object {
        const val PACT_CONSUMER: String = "Mobile App"
        val DEFAULT_REQUEST_HEADERS: Map<String, String> = MapUtils.putAll(
            HashMap<String, String>(),
            arrayOf(
                arrayOf("x-correlation-id", "x-correlation-id"),
                arrayOf("x-channelId", "x-correlation-id"),
                arrayOf("x-defaultServiceId", "x-correlation-id")
            )
        )!!
        val DEFAULT_RESPONSE_HEADERS: Map<String, String> = MapUtils.putAll(
            HashMap<String, String>(),
            arrayOf(arrayOf("Content-Type", "application/json; charset=utf-8"))
        )!!
    }
}